from django.shortcuts import render

# Create your views here.
import json

from django.http import HttpResponse
import string


def index(request):
	with open('../thescore_test/nflrush/rushing.json') as json_file:
		data = json.load(json_file)
		for p in data:
			for old_key in p.keys():
				if not old_key.isalnum():
					if '+' in old_key:
						new_key = old_key.replace('+', '_plus')
						p[new_key] = p.pop(old_key)
					elif '/' in old_key:
						new_key = old_key.replace('/', '_')
						p[new_key] = p.pop(old_key)
					elif '%' in old_key:
						new_key = old_key.replace('%', '_pct')
						p[new_key] = p.pop(old_key)
				if old_key in ('Yds', ):
					exclude = set(',')
					p[old_key] = int(''.join(ch for ch in str(p[old_key]) if ch not in exclude))

	context= {
	'data': data,
	}

	return render(request, 'nflrush/index.html', context)
